package com.animoz;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.animoz.modele.Animal;

public class AnimalAvecPopulationDePlusDeTrenteApplication {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("animaux");
		EntityManager em = emf.createEntityManager();

		try {
			List<Animal> animaux = em.createQuery("select distinct a from Animal a join a.populations p where p.taille > 30", Animal.class)
			                         .getResultList();
			
			for (Animal a: animaux) {
				System.out.println(a.getNom());
			}
		} finally {
			em.close();
			emf.close();
		}
	}

}
