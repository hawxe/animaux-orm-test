package com.animoz;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class SuppressionEspeceApplication {
	
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("animaux");
		EntityManager em = emf.createEntityManager();
		
		try {
			em.getTransaction().begin();
			em.createQuery("delete from Espece e where e.nom = :nom")
			  .setParameter("nom", "Hominidé")
			  .executeUpdate();
			em.getTransaction().commit();
		}finally {
			em.close();
			emf.close();
		}
	}

}
