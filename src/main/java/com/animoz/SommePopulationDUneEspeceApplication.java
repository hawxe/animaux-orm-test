package com.animoz;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class SommePopulationDUneEspeceApplication {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("animaux");
		EntityManager em = emf.createEntityManager();

		try {
			String nom = "Félidé";

			long somme = em.createQuery("select sum(p.taille) from Population p where p.animal.espece.nom = :nomEspece", Long.class)
					       .setParameter("nomEspece", nom)
					       .getSingleResult();

			System.out.println(somme);
			
		} finally {
			em.close();
			emf.close();
		}
	}

}
