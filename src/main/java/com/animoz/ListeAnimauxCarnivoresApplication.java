package com.animoz;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.animoz.modele.Animal;
import com.animoz.modele.Regime;

public class ListeAnimauxCarnivoresApplication {
	
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("animaux");
		EntityManager em = emf.createEntityManager();
		
		try {
			List<Animal> animaux = em.createQuery("select a from Animal a where a.regime = :regime order by a.nom", Animal.class)
					                 .setParameter("regime", Regime.carnivore)
					                 .getResultList();
			for(Animal animal : animaux) {
				System.out.println(animal.getNom() + " " + animal.getDescription());
			}
		}finally {
			em.close();
			emf.close();
		}
	}

}
