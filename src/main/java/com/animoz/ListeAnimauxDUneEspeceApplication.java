package com.animoz;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.animoz.modele.Animal;
import com.animoz.modele.Espece;

public class ListeAnimauxDUneEspeceApplication {
	
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("animaux");
		EntityManager em = emf.createEntityManager();
		
		try {
			Espece espece = em.find(Espece.class, 1L);
			
			for (Animal a : espece.getAnimaux()) {
				System.out.println(a.getNom());
			}
			
		}finally {
			em.close();
			emf.close();
		}
	}

}
